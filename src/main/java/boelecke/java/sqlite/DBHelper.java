package boelecke.java.sqlite;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBHelper 
{
	private Connection connection = null;
	
	
	/*-----------------------------Constructor---------------------------------------------*/
	public DBHelper() throws ClassNotFoundException
	{
		// loading sqlite driver
		Class.forName("org.sqlite.JDBC");
	}
	
	
	
	
	/*-----------------------------Methods-------------------------------------------------*/
	private void openConnection() throws SQLException
	{
		connection = DriverManager.getConnection("jdbc:sqlite:gdt.db");
	}
	
	public void createTable() throws SQLException
	{
		
		openConnection();
		
		
		Statement statement = connection.createStatement();
	
		String sql = "create table if not exists gdt(id text unique, field text, obj text);";
		statement.execute(sql);
		
		
		
		connection.close();	
	}
	
	public void createTable(String tableName, String[] columns) throws SQLException
	{
		
		openConnection();
		
		
		Statement statement = connection.createStatement();
	
		StringBuilder strBuilder = new StringBuilder();
		
		
		for(int i = 0 ; i<= columns.length-1; i++)
		{
			if(i== columns.length-1)
			{
				strBuilder.append(columns[i]+",");
			}
			else
			{
				strBuilder.append(columns[i]+",");
			}
				
		}
		
		
		
		String sql = "create table if not exists"+tableName+ "("+ strBuilder.toString()+");";
		statement.execute(sql);
		
		connection.close();	
	}
	
	
	
	public void fillTable(Class<?> clazz) throws SQLException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, InstantiationException
	{
		openConnection();
		// mirror, mirror,.... let's do some reflection magic
		
		Class<?> cl = clazz;
		
		Field[] fields = cl.getDeclaredFields();
		
		ArrayList<String> fieldNames = new ArrayList<String>();
		
		for(Field field : fields)
		{
			if(!Modifier.isStatic(field.getModifiers()))
			{
				fieldNames.add(field.getName());
				
			}
		}
		
		for(String s :fieldNames)
		{
			String field = s.toUpperCase();
		
			Field dField = cl.getDeclaredField(field);
			dField.setAccessible(true);
			String id = (String) dField.get(null);
		
			String sql = "INSERT OR IGNORE INTO gdt VALUES ('"+id+"','"+s+"','"+cl.getCanonicalName()+"')";
		
			Statement statement = connection.createStatement();
		
			statement.executeUpdate(sql);
		}
		
		connection.close();
	}
	
	public String[] getEntry(String id) throws SQLException
	{
		openConnection();
		
		Statement statement = connection.createStatement();
		
		String sql = "SELECT * FROM gdt WHERE id='"+id+"'";
		ResultSet result = statement.executeQuery(sql);
		
		String[] resultArray = new String[3];
		
		
		while (result.next())
		{
			resultArray[0] = result.getString(1);
			resultArray[1] = result.getString(2);
			resultArray[2] = result.getString(3);
		}
		connection.close();
		return resultArray;
	}




	public ArrayList<String[]> getEntry(String where, String value) throws SQLException
	{
		openConnection();
		
		Statement statement = connection.createStatement();
		
		String sql = "SELECT * FROM gdt WHERE "+where+"='"+ value/*.substring(value.lastIndexOf(".")+1)*/+"'";
		ResultSet result = statement.executeQuery(sql);
		
		
		ArrayList<String[]> resultList  = new ArrayList<String[]>();
		
		while (result.next())
		{
			

			resultList.add(new String[]{result.getString(1),result.getString(2),result.getString(3)});
		}
		connection.close();
		return resultList;
	}
	
	
	
	
	
	
	

}
