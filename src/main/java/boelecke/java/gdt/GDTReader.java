package boelecke.java.gdt;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Scanner;

import boelecke.java.gdt.xdtobjectd.*;
import boelecke.java.sqlite.DBHelper;

public class GDTReader 
{
	
	/*public static void main(String[] args) 
	{
		GDTReader reader = new GDTReader("C:\\Users\\admin\\Desktop\\ADC\\SAMADIGI.gdt");
		try 
		{
			ArrayList<Object> message = reader.read();
			
			
			GDTWriter writer = new GDTWriter();
			writer.write(message);
			
			
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
/*-------------------------------------- Variables ------------------------------------------------*/	
	private String path;


/*-------------------------------------- COnstructor ----------------------------------------------*/
	public GDTReader(String path)
	{
		this.path = path;
	}
	
	
/*-------------------------------------- Methods ---------------------------------------------------*/
	public ArrayList<Object> read() throws Exception
	{
		
		String line ;
		File gdtFile = new File(path);
		Scanner scanner = new Scanner(gdtFile);
		
		DBHelper dbHelper = new DBHelper();
		
		
		dbHelper.createTable();
		// create db with gdt data
		Class<?>[] objArray = {ObjKopfdaten.class, ObjPatient.class, Head.class, ObjAnforderung.class };
		
		
		
		
		
		
		for(Class<?> c : objArray)
		{
			dbHelper.fillTable(c);
		}
	
		
		ArrayList<Object> gdtMessage = new ArrayList<Object>();
		
		// read in file
		while (scanner.hasNextLine())
		{
			line = scanner.nextLine();
			int length = Integer.parseInt(line.substring(0, 3));
			String field = line.substring(3,7);
			String content;
			
						
			if (line.length() !=  length-2)
			{
				scanner.close();
				throw new Exception("Length in GDT file does not match real length");
			}
			else
			{
				content = line.substring(7);
			}
			
			String[] result = dbHelper.getEntry(field);
			
			if (result[0] != null && result != null)
			{
				Class<?> objClass = Class.forName(result[2]);
				int index = -1;	
				
				for (Object o : gdtMessage)
				{
					if( objClass.isAssignableFrom(o.getClass()))
					{
						index = gdtMessage.indexOf(o);
						break;
					}
				}
				
				if ( index != -1)
				{}
				else
				{
					gdtMessage.add(objClass.newInstance());
					index = gdtMessage.size()-1;
				}
				// use of reflection to invoke setXXX-method
				String variable = result[1].substring(0, 1).toUpperCase() + result[1].substring(1);
				Method setMethod = gdtMessage.get(index).getClass().getMethod("set"+variable, String.class);
				setMethod.invoke(gdtMessage.get(index), content);
			}
		}
		scanner.close();
		return gdtMessage;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
