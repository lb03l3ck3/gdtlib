package boelecke.java.gdt;

import java.util.ArrayList;

import boelecke.java.gdt.xdtobjectd.XDTObject;

public class GDTWriter 
{
	public GDTWriter()
	{
		
	}
	
	
	/*-------------------------------------Methods-------------------------------------------------------------*/
	 
	 
	public String write(ArrayList<Object> gdtMessage) throws Exception
	{
		
	
		StringBuilder strBuilder = new StringBuilder();
		
		
		for(Object obj: gdtMessage)
		{
			ArrayList<String> out = ((XDTObject) obj).toGDT();
			
			for(String s: out)
			{
				strBuilder.append(s +"\r\n");
			}
			
			
			
		}
	
		String outputString = strBuilder.toString();
		
		return outputString;
	
	}
	
	
	
	 
}
