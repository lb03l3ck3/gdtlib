package boelecke.java.gdt.xdtobjectd;

import java.lang.reflect.Method;
import java.util.ArrayList;

import boelecke.java.sqlite.DBHelper;

public abstract class  XDTObject 
{
	public ArrayList<String> toGDT() throws Exception
	{
		
			
			DBHelper dbHelper = new DBHelper();
			int length;
			
			String content = null;
			String line;
			
					
			ArrayList<String[]> resultList = dbHelper.getEntry("obj",this.getClass().getCanonicalName());
			ArrayList<String> gdt = new ArrayList<String>();
			
			
			for(String[] result: resultList)
			{
				if (result[0] != null && result != null)
				{
				
					// use of reflection to invoke setXXX-method
					String variable = result[1].substring(0, 1).toUpperCase() + result[1].substring(1);
					Method setMethod = this.getClass().getMethod("get"+variable);
					content  = (String) setMethod.invoke(this);
					if(content != null)
					{
						length = content.length() + 3 + 4 + 2; //2 char for line end 4 char for field and 3 char for line length
						String lString = String.valueOf(length);
						if(lString.length()< 3)
						{
							if(lString.length() == 1)
							{
								lString = "00"+lString;
							}
							else
							{
								lString = "0"+lString;
							}
						}
						 line = lString+result[0]+content;
						gdt.add(line);
					}
					
				}
			}
			
			
			
			
			
			
			return gdt;
			
			
			
			
			
			
		

	}
	
}
