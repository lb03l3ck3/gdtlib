package boelecke.java.gdt.xdtobjectd;

public class ObjKopfdaten extends XDTObject
{
	private String idRecipient;
	private String idSender;
	private String date;
	private String format;
	private String version;
	private String responsibleForSoftware;
	private String Software;
	private String releaseSoftware;
	
	final static String IDRECIPIENT				= "8315";
	final static String IDSENDER				= "8316";
	final static String DATE					= "9103";
	final static String FORMAT					= "9206";
	final static String VERSION					= "9218";
	final static String RESPONSIBLEFORSOFTWARE	= "0102";
	final static String SOFTWARE				= "0103";
	final static String RELEASESOFTWARE			= "0132";
	final static String OBJEKTIDENT				= "8200";
	
	
	public ObjKopfdaten()
	{
		
	}
	
	public String getIdRecipient() {
		return idRecipient;
	}
	public void setIdRecipient(String idRecipient) {
		this.idRecipient = idRecipient;
	}
	public String getIdSender() {
		return idSender;
	}
	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getResponsibleForSoftware() {
		return responsibleForSoftware;
	}
	public void setResponsibleForSoftware(String responsibleForSoftware) {
		this.responsibleForSoftware = responsibleForSoftware;
	}
	public String getSoftware() {
		return Software;
	}
	public void setSoftware(String software) {
		Software = software;
	}
	public String getReleaseSoftware() {
		return releaseSoftware;
	}
	public void setReleaseSoftware(String releaseSoftware) {
		this.releaseSoftware = releaseSoftware;
	}

}
