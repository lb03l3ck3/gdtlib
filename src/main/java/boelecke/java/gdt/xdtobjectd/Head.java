package boelecke.java.gdt.xdtobjectd;

public class Head extends XDTObject 
{
	
	private String type;
	private String length;
	
	private final static String TYPE = "8000";
	private final static String LENGTH = "8100";
	
	
	
	public Head() 
	{
		
	}

	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getLength() {
		return length;
	}



	public void setLength(String length) {
		this.length = length;
	}
	
	

	
	
	

	
	
	
	
}
