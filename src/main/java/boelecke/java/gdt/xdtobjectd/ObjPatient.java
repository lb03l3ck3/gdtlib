package boelecke.java.gdt.xdtobjectd;

public class ObjPatient extends XDTObject 
{
	private String patientID;
	private String suffix;
	private String forename;
	private String name;
	private String birthday;
	private String title;
	private String insuranceNumber;
	private String residence;
	private String street;
	private String insuranceType;
	private String sex;
	private String postalCode;
	private String mobile;
	private String email;
	private String phone;
	private String language;
	
	
	
	final static String PATIENTID 		= "3000";
	final static String SUFFIX			= "3100";
	final static String FORENAME		= "3102";
	final static String NAME			= "3101";
	final static String BIRTHDAY		= "3103";
	final static String TITLE			= "3104";
	final static String INSURANCENUMBER	= "3105";
	final static String RESIDENCE		= "3106";
	final static String STREET			= "3107";
	final static String INSURANCETYPE	= "3108";
	final static String SEX				= "3110";
	final static String POSTALCODE		= "3112";
	final static String MOBILE			= "3618";
	final static String EMAIL			= "3619";
	final static String PHONE			= "3626";
	final static String LANGUAGE		= "3628";
	
	public ObjPatient()
	{
		
	}

	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInsuranceNumber() {
		return insuranceNumber;
	}

	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}

	public String getResidence() {
		return residence;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	
	
	
	
	
	
}
