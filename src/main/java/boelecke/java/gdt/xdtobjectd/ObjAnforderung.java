package boelecke.java.gdt.xdtobjectd;

public class ObjAnforderung extends XDTObject {

	
	private String dayOfTreatmentData;
	private String timeOfTreatmentData;
	private String typeOfProcedure;
	private String testIdent;
	private String dateOfData;
	private String timeOfData;
	private String comment;
	
	public static final String DAYOFTREATMENTDATA	= "6200";
	public static final String TIMEOFTREATMENTDATA 	= "6201";
	public static final String TYPEOFPROCEDURE		= "8402";
	public static final String TESTIDENT			= "8410";
	public static final String DATEOFDATA			= "8432";
	public static final String TIMEOFDATA			= "8439";	
	public static final String COMMENT 				= "6227";
	
	public ObjAnforderung() 
	{
	}


	public String getDayOfTreatmentData() {
		return dayOfTreatmentData;
	}


	public void setDayOfTreatmentData(String dayOfTreatmentData) {
		this.dayOfTreatmentData = dayOfTreatmentData;
	}


	public String getTimeOfTreatmentData() {
		return timeOfTreatmentData;
	}


	public void setTimeOfTreatmentData(String timeOfTreatmentData) {
		this.timeOfTreatmentData = timeOfTreatmentData;
	}


	public String getTypeOfProcedure() {
		return typeOfProcedure;
	}


	public void setTypeOfProcedure(String typeOfProcedure) {
		this.typeOfProcedure = typeOfProcedure;
	}


	public String getTestIdent() {
		return testIdent;
	}


	public void setTestIdent(String testIdent) {
		this.testIdent = testIdent;
	}


	public String getDateOfData() {
		return dateOfData;
	}


	public void setDateOfData(String dateOfData) {
		this.dateOfData = dateOfData;
	}


	public String getTimeOfData() {
		return timeOfData;
	}


	public void setTimeOfData(String timeOfData) {
		this.timeOfData = timeOfData;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	

}
