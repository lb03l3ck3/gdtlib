import static org.junit.Assert.*;

import java.awt.image.SampleModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import boelecke.java.gdt.GDTReader;
import boelecke.java.gdt.GDTWriter;


public class TestCaseWriter 
{
	GDTWriter writer;
	ArrayList<String> scannedText = new ArrayList<String>();
	ArrayList<Object> gdtMessage;
	String writerOutput;
	
	
	
	@Before public void prepareTest()
	{
		// initialize test subject
		writer = new GDTWriter();
		
		Scanner scanner = null;
		
				URL sampleURL = this.getClass().getResource("/SAMADIGI.gdt");
				File sampleFile;
				try 
				{
					sampleFile = new File(sampleURL.toURI());
					GDTReader reader = new GDTReader(sampleFile.getPath());
					gdtMessage = reader.read();
					
					scanner = new Scanner(sampleFile);
					while(scanner.hasNext())
					{
						scannedText.add(scanner.next());
					}
					
					
				} 
				catch (URISyntaxException e) 
				{
					fail("Could not find the samplefile");
					e.printStackTrace();
				} catch (Exception e) {
					fail("Prolems while reading GDT File");
					e.printStackTrace();
				}
				finally
				{
					scanner.close();
				}
	}
	
	@Test public void testWrite() 
	{	
		
		try 
		{
			writerOutput=writer.write(gdtMessage);
		} 
		catch (Exception e) 
		{
			fail("error while writing file");
			e.printStackTrace();
		}
		
		
		for (String s : scannedText)
		{
			
			
			
			assertTrue("Writeroutput does not contain "+ s,writerOutput.contains(s));
			
		}
		
		
		
		
		
		
	}

	
}
